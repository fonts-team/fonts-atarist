# Example file for upstream/metadata.
# See https://wiki.debian.org/UpstreamMetadata for more info/fields.
# Below an example based on a github project.

# Bug-Database: https://github.com/<user>/fonts-atarist/issues
# Bug-Submit: https://github.com/<user>/fonts-atarist/issues/new
# Changelog: https://github.com/<user>/fonts-atarist/blob/master/CHANGES
# Documentation: https://github.com/<user>/fonts-atarist/wiki
# Repository-Browse: https://github.com/<user>/fonts-atarist
# Repository: https://github.com/<user>/fonts-atarist.git
